from preprocess import *
from sklearn.model_selection import train_test_split
import torch
from model import *
import os
import pickle

path = os.path.dirname(os.path.realpath("__file__")[:-3])
print(path)

# data loading
doc = load(path)

# Creating instances of the dataset
instances = create_examples(doc)

#save all necessary files
instances_file = open(str(path) + "/data/instances.pkl", "wb")
pickle.dump(instances, instances_file)
instances_file.close()

output_file = open(str(path) + '/data/instances.txt', 'w')
output_file.write(str(instances))
output_file.close()

# Word embeddings
with open(path + '/data/instances.pkl','rb') as f:
    instances =  pickle.load(f)
model = word2vec_embeddings(instances, sg = True, negative = 0, hs = True, alpha = 0.05)
embedding_matrix = model.syn1
np.save(path + '/data/embedding_matrix' ,embedding_matrix)

# Creating the dictionary for the unique words 
with open(path + '/data/instances.pkl','rb') as f:
    instances =  pickle.load(f)
all_words = vocab(instances)
all_words_file = open(str(path) + "/data/all_words.pkl", "wb")
pickle.dump(all_words, all_words_file)
all_words_file.close()

with open(path + '/data/all_words.pkl','rb') as f:
    all_words =  pickle.load(f)
word_index = create_unique_word_dict(all_words)
word_index_file = open(str(path) + "/data/word_index.pkl", "wb")
pickle.dump(word_index, word_index_file)
word_index_file.close()

# Find max context and question lengths for padding
lens_cont, lens_q = lengths(instances)
max_context = max(lens_cont)
max_question = max(lens_q)

# Create training and evaluation set
data_xy = create_xy(instances, word_index, 300 , max_question)
data_file = open(str(path) + "/data/data_xy.pkl", "wb")
pickle.dump(data_xy, data_file)
data_file.close()

# Modeling
x_train ,x_eval = train_test_split(data_xy,test_size=0.2)
x_data = torch.cat([v[0] for v in x_train])
x_data = x_data.view(len(x_train[0][0]), -1)

ntokens = len(all_words)  # size of vocabulary
emsize = 100  # embedding dimension
d_hid = 50  # dimension of the feedforward network model in nn.TransformerEncoder
nlayers = 2  # number of nn.TransformerEncoderLayer in nn.TransformerEncoder
nhead = 2  # number of heads in nn.MultiheadAttention
dropout = 0.2  # dropout probability
model = TransformerModel(ntokens, emsize, nhead, d_hid, nlayers, dropout)

criterion = nn.CrossEntropyLoss()
lr = 5.0  # learning rate
optimizer = torch.optim.Adam(model.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)
bptt = 64
#train(model)

######## Bi -ltsm attempt ######

