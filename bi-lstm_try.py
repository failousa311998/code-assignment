
import numpy as np
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional
from keras.datasets import imdb 
from keras.layers import *
from keras.models import *
from keras import backend as K
import os
import random
import pickle
from sklearn.model_selection import train_test_split
from scipy.special import softmax
import matplotlib.pyplot as plt

path = os.path.dirname(os.path.realpath("__file__")[:-3])
print(path)


class attention(Layer):
    def __init__(self, return_sequences=True):
        self.return_sequences = return_sequences

        super(attention,self).__init__()

    def build(self, input_shape):
        self.W=self.add_weight(name="att_weight", shape=(input_shape[-1],1), initializer="normal")
        self.b=self.add_weight(name="att_bias", shape=(input_shape[1],1),initializer="normal")
        self.b=self.add_weight(name="att_bias", shape=(input_shape[1],1))
        self.b=self.add_weight(name="att_bias", shape=(input_shape[1],1))
        super(attention,self).build(input_shape)


    def call(self, x):
        e = K.tanh(K.dot(x,self.W)+self.b)
        a = K.softmax(e, axis=1)
        output = x*a
        if self.return_sequences:
            return output
        return K.sum(output, axis=1)

    def build(self, input_shape):
        self.W=self.add_weight(name="att_weight", shape=(input_shape[-1],1), initializer="normal")
        self.b=self.add_weight(name="att_bias", shape=(input_shape[1],1),
                               initializer="zeros")

with open(path + '/data/data_xy.pkl','rb') as f:
    data =  pickle.load(f)

with open(path + '/data/all_words.pkl','rb') as f:
    all_words =  pickle.load(f)

new_data=[]
for d in data:
    if len(d[0]) == 332 & len(d[1]) == 332:
        new_data.append(d)

train , eval = train_test_split(new_data,test_size=0.2)

## Pick random 1000 training samples for faster run
train = random.sample(train, 1000)
eval = random.sample(eval, 20)

### Training data 
x_data = np.array([np.array(v[0], dtype = np.int) for v in train])
y_data = np.array([np.array(v[1], dtype = np.int) for v in train])
y_data = np.concatenate(y_data, axis = 0).reshape(1000,332)

### Test data
x_test = np.array([np.array(v[0], dtype = np.int) for v in eval])
y_test = np.array([np.array(v[1], dtype = np.int) for v in eval])
y_test = np.concatenate(y_test, axis = 0).reshape(len(y_test),332)

##### Modeling with attention 
model2 = Sequential()
model2.add(Embedding(len(all_words), 128, input_length=332))
model2.add(Bidirectional(LSTM(64, return_sequences=False)))
model2.add(attention(return_sequences=False)) # receive 3D and output 3D
model2.add(Dropout(0.5))
model2.add(Dense(y_data.shape[1], activation='sigmoid'))
model2.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy']) 
model2.summary()
mod = model2.fit(x_data, y_data,
           batch_size=128,
           epochs=25)

model_file = open(str(path) + "/data/model.pkl", 'wb')
weigh= model2.get_weights()    
pickle.dump(weigh, model_file)
model_file.close()

loss = mod.history['loss']
plt.plot(loss)
plt.title('Loss')
plt.savefig(str(path) + "/data/loss.png")

## Evaluation 
predicted_mask = model2.predict([x_test, y_test])

score = []
for pred, y_real in zip(predicted_mask,y_test):
    m = max(softmax(pred))
    if (1 in y_real.tolist()):
        if (softmax(pred).tolist().index(m) == y_real.tolist().index(1)):
            score.append(1)
    else:
        score.append(0)


Accuracy = sum(score)/len(score)*100
print('Evaluation score was:',  Accuracy, "%")

