import matplotlib.pyplot as plt
from preprocess import lengths
from main import instances

lens_cont, lens_q = lengths(instances)

plt.hist(lens_cont, bins = 30)
plt.title("Context lengths")
plt.show()

plt.hist(lens_q, bins = 30, color= "green")
plt.title("Question lengths")
plt.show()
