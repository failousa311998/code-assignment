import json
import nltk 
import re
import numpy as np
import torch
from gensim.models import Word2Vec

def load(path):
    """ Loading the json file

    Returns:
        [dict]: A dictionary with keys : ['version', 'data']
    """
    with open(str(path) + '/data/dev-v2.0.json', 'r') as f:
       doc = json.load(f)
    return doc
    
def tokenize(sentence: str):
    """ Word tokenization of a sentence

    Args:
        sentence (str): A string with the sentence to be tokenized

    Returns:
        (list): A list containing tokenized words
    """
    return nltk.word_tokenize(sentence)

def preprocess(sentence: str):
    """ Cleaning the data from special characters and punctuations and convert words to lowercase.

    Args:
        sentence (str): The sentence to be preprocessed

    Returns:
        (str): The cleaned sentence
    """
    #sentence = str(sentence)
    sentence = sentence.replace("'s ", ' ')
    cleanString = re.sub(r'[^\w\s]+|_', ' ', sentence, flags=re.U)
    #stop_words = set(stopwords.words('english')) 
    word_tokens = tokenize(cleanString)
    filtered_sentence = [w for w in word_tokens]
    return " ".join(filtered_sentence)

def getToken_index(str: str, i: int):
    """ Finds the start index of a word or phrase inside a list of tokenized words

    Args:
        str (str): The sentence to be tokenized
        i (int): the start index of the word/phrase inside the string sentence

    Returns:
        (int): The start index in the tokenized sentence
    """
    str = preprocess(str)
    if str[i] == ' ':  # if whitespace, return white space
        return str[i]
    return len(tokenize(str[:i])) 

def create_examples(doc: dict):
    """ Creates instances of the dataset

    Args:
        doc (dict): A dictionary with keys: ['version', 'data']. 
                    -doc['version'] is a string denoting the version of the dataset.
                    -doc['data'] is a list containing dictionaries with keys: ['title', 'paragraphs']
                        -Each title is a string denoting the title of the paragraph
                        -Each paragraph is a dictionary with keys: ['qas', 'context']
                            -Each qas is a dictionary with keys: 'plausible_answers', 'question', 'id', 'answers', 'is_impossible']
                            -Each context is a string of sentences
                        
    Returns:
        (list): A list of dictionaries with keys: ['index', 'id', 'context', 'question', 'impossible', 'answers', 'answer_starts', 'answer_ends', 'mask']
    """
    
    instances =[]
    for topic in doc["data"]: 
        for paragraph in topic["paragraphs"]: 
            context = preprocess(paragraph["context"])
            for qna in paragraph["qas"]:
                question = preprocess(qna["question"])
                id = qna["id"]
                # if 'plausible_answers' in qna.keys():
                #     type = 'plausible_answers'
                # else:
                #     type = 'answers' 
                impossible = int(qna["is_impossible"])
                answers, answer_starts, answer_ends, masks = [], [], [], []
                for ans in qna["answers"]:
                    
                    if ans["text"] != '.':
                        answer = ans["text"] 
                        pre_ans = preprocess(answer)
                        answer_start = getToken_index(context, context.find(pre_ans))  
                        answer_end = answer_start + len(tokenize(pre_ans)) - 1 
                        mask = np.zeros(len(tokenize(context)), dtype = np.float32)
                        mask[answer_start:answer_end+1] = 1
                        masks.append(mask)
                        answers.append(tokenize(pre_ans))
                        answer_starts.append(answer_start)
                        answer_ends.append(answer_end)
                instance = {
                            'index': len(instances),
                            'id': id,
                            'context': tokenize(context),
                            'question': tokenize(question),
                            'impossible': impossible,
                            'answers': answers,
                            'answer_starts': answer_starts,
                            'answer_ends': answer_ends,
                            'mask': masks
                            }
                instances.append(instance)
    return instances

def word2vec_embeddings(instances: list, sg: bool, negative: int, hs: bool, alpha: float):
    """ A function that fits the word2vec pre-trained model to the distinct words of our vocabulary

    Args:
        instances (list): A list of dictionaries with keys: ['index', 'id', 'context', 'question', 'impossible', 'answers', 'answer_starts', 'answer_ends', 'mask']
        sg (boolean): Choose training algorithm: True for skip-gram; False for CBOW.
        hs (boolean): True for hierarchical softmax; False for negative sampling 
        negative (int): if > 0, negative sampling will be used,
                        the int for negative specifies how many “noise words” should be drawn 
                        (usually between 5-20). If set to 0, no negative sampling is used.
        alpha (float): The initial learning rate

    Returns:
        (gensim.models.word2vec.Word2Vec): The final model that contains the word embeddings 
    """
    x = []
    sg = int(sg)
    hs = int(hs)
    if hs == 0 : negative = 0
    for ex in instances: 
        n = []
        for ans in ex['answers']: 
            for w in ans: 
                n.append(w)
        new = ex["context"] + ex["question"] + n
        x.append(new)
    model = Word2Vec(x, min_count = 1, sg = sg, negative = negative, hs = hs, alpha = alpha)

    return model

def lengths(instances: list):
    """ Computes contexts' and questions' different lengths

    Args:
        instances (list): A list of dictionaries with minimum keys: ['context', 'question']

    Returns:
        (list , list): Lists of integers, denoting context and question lengths respectively
    """

    lens_cont, lens_q = [], []
    for instance in instances: 
        lens_cont.append(len(instance["context"]))
        lens_q.append(len(instance["question"]))

    return lens_cont, lens_q

def keep_relevant(context: list, question:list, model, i: float):
    """ Keep words from the context that have a cosine similarity greater than i with words in question

    Args:
        context (list): A list of tokenized words of the context
        question (list): A list of tokenized words of the question
        model (gensim.models.word2vec.Word2Vec): a pre-trained word2vec model containing the word wmbeddings
        i (float): The minimum cosine similarity

    Returns:
        (list): A list containing new context tokenized words
    """
    new_context =[]
    for c in context:
        for q in question: 
             if model.wv.similarity(c, q) > i:
                new_context.append(c)
                break
    return new_context

def vocab(instances: list) -> list:
    """ Returns a set of all the words

    Args:
        instances (list): A list of dictionaries with minimum keys: ['context', 'question', 'answers']

    Returns:
        list: A list containg all the words of the vocabulary
    """
    all_words = [] 
    for example in instances:
        for cnxt in example["context"]:
            all_words.append(cnxt)
        for q in example["question"]:
            all_words.append(q)     
        for a in example["answers"]:
            for w in a:
                all_words.append(w)

    all_words = sorted(set(all_words))
    return all_words

def create_unique_word_dict(text: list) -> dict:
    """
    A method that creates a dictionary where the keys are unique words
    and key values are indices
    Args:
        text (list): A list containing all the words
    Returns:
        (dict): A dictionary with keys the unique words and values the corresponding indices
    """
    # Getting all the unique words from our text and sorting them alphabetically
    words = list(set(text))
    words.sort()

    # Creating the dictionary for the unique words
    unique_word_dict = {}
    for i, word in enumerate(words):
        unique_word_dict.update({
            word: i
        })

    return unique_word_dict 

def create_xy(instances: list, word_index: dict, max_context: int, max_question: int) -> list:
    """ Creates a list of tuples (x,y) where x is the training data and y the target.
     x and y are padded or clipped in order to have the same size

    Args:
        instances (list): A list of dictionaries with keys: ['index', 'id', 'context', 'question', 'impossible', 'answers', 'answer_starts', 'answer_ends', 'mask']
        word_index (dict): A dictionary with keys the unique words and values the corresponding indices
        max_context (int): The desirable size of the contexts
        max_question (int): The desirable size of the questions

    Returns:
        list: A list of tuples (x,y) where x and y are tensors with shape (max_context + max_question)
    """

    batches = []
    for example in instances: 
        cont_embeddings, quest_embeddings = [], []
        for w1 in example["context"]:
            cont_embeddings.append(word_index[w1])
        for w2 in example['question']:
            quest_embeddings.append(word_index[w2])
        diff = max_context - len(cont_embeddings)
        if diff >= 0:
            for _ in range(diff): 
                cont_embeddings.append(0)  
        else:
            cont_embeddings[0:max_context+1] 
        diff = max_question - len(quest_embeddings)
        if diff >= 0:
            for _ in range(diff):
                quest_embeddings.append(0)
        else:
             quest_embeddings[0:max_question+1] 
        x = torch.tensor(cont_embeddings + quest_embeddings, dtype = int)             
        if len(example['mask']) == 0: 
            w3 = torch.zeros(len(x),)
            batches.append((x, w3))
        else:           
            for w3 in example['mask']: 
                diff = len(x) - len(w3) 
                if diff < 0:
                    w3 = w3[0:len(x)+1] 
                else:
                    w3 = np.append(w3, np.zeros(diff))         
                wf = torch.tensor(w3, dtype = int)
                if len(wf) == 632: print('wrong')
                batches.append((x, wf))

    return batches

